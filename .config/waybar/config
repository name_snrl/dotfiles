{
    // "layer": "top", // Waybar at top layer
    "position": "bottom", // Waybar position (top|bottom|left|right)
    // "height": 30, // Waybar height (to be removed for auto height)
    // "width": 1280, // Waybar width
    // Choose the order of the modules
    "modules-left": ["sway/workspaces", "sway/mode"],
    "modules-center": ["sway/window"],
    "modules-right": ["idle_inhibitor", "pulseaudio", "network", "custom/vpn", "cpu", "memory", "temperature", "battery", "disk", "disk#sda", "custom/btc", "custom/weather", "clock", "custom/apps_menu", "tray"],

    //Modules configuration
    
    //"sway/workspaces": {
    //    "disable-scroll": true,
    //    "all-outputs": true,
    //    "format": "{name}",
    //},
    "sway/mode": {
        "format": "<span style=\"italic\">{}</span>"
    },
    "mpd": {
        "format": "{stateIcon} {consumeIcon}{randomIcon}{repeatIcon}{singleIcon}{artist} - {album} - {title} ({elapsedTime:%M:%S}/{totalTime:%M:%S}) ⸨{songPosition}|{queueLength}⸩ ",
        "format-disconnected": "Disconnected ",
        "format-stopped": "{consumeIcon}{randomIcon}{repeatIcon}{singleIcon}Stopped ",
        "unknown-tag": "N/A",
        "interval": 2,
        "consume-icons": {
            "on": " "
        },
        "random-icons": {
            "off": "<span color=\"#f53c3c\"></span> ",
            "on": " "
        },
        "repeat-icons": {
            "on": " "
        },
        "single-icons": {
            "on": "1 "
        },
        "state-icons": {
            "paused": "",
            "playing": ""
        },
        "tooltip-format": "MPD (connected)",
        "tooltip-format-disconnected": "MPD (disconnected)"
    },
    "idle_inhibitor": {
        "format": "{icon}",
        "format-icons": {
            "activated": "",
            "deactivated": ""
        }
    },
    "tray": {
        // "icon-size": 21,
        "spacing": 10
    },
    "clock": {
        // "timezone": "America/New_York",
        "tooltip-format": "<big>{:%Y %B}</big>\n<tt><small>{calendar}</small></tt>",
        "format-alt": "{:%Y-%m-%d}"
    },
    "cpu": {
        "format": "{usage}% ",
        "on-click": "gnome-system-monitor"
    },
    "memory": {
        "format": "{percentage}% ",
        "on-click": "gnome-system-monitor"
    },
    "temperature": {
        "thermal-zone": 3,
        "hwmon-path": "/sys/class/hwmon/hwmon3/temp1_input",
        "critical-threshold": 80,
        // "format-critical": "{temperatureC}°C {icon}",
        "format": "{temperatureC}°C {icon}",
        "format-icons": ["", "", ""],
        "on-click": "myterm -p -- bash -c 'sensors|less'"
    },
    "backlight": {
        // "device": "acpi_video1",
        "format": "{percent}% {icon}",
        "format-icons": ["", ""]
    },
    "battery": {
        "states": {
            // "good": 95,
            "warning": 30,
            "critical": 15
        },
        "format": "{capacity}% {icon}",
        "format-charging": "{capacity}% ",
        "format-plugged": "{capacity}% ",
        "format-alt": "{time} {icon}",
        // "format-good": "", // An empty format will hide the module
        // "format-full": "",
        "format-icons": ["", "", "", "", ""],
        "on-click-right": "gnome-control-center power",
        "on-click-middle": "toggle-devices"
    },
    "network": {
        // "interface": "wlp2*", // (Optional) To force the use of this interface
        "format-wifi": "{essid} ({signalStrength}%) ",
        "format-ethernet": "{ifname}: {ipaddr}/{cidr} ",
        "format-linked": "{ifname} (No IP) ",
        "format-disconnected": "Disconnected ⚠",
        "format-alt": "{ifname}: {ipaddr}/{cidr}"
    },
    "pulseaudio": {
        // "scroll-step": 1, // %, can be a float
        "format": "{volume}% {icon} {format_source}",
        "format-bluetooth": "{volume}% {icon} {format_source}",
        "format-bluetooth-muted": " {icon} {format_source}",
        "format-muted": " {format_source}",
        "format-source": "{volume}% ",
        "format-source-muted": "",
        "format-icons": {
            "headphone": "",
            "hands-free": "",
            "headset": "",
            "phone": "",
            "portable": "",
            "car": "",
            "default": ["", "", ""]
        },
        "on-click": "pavucontrol",
        "on-click-right": "blueman-manager",
        "on-click-middle": "pulseaudio-equalizer-gtk"
    },
    "custom/media": {
        "format": "{icon} {}",
        "return-type": "json",
        "max-length": 40,
        "format-icons": {
            "spotify": "",
            "default": "🎜"
        },
        "escape": true,
        "exec": "$HOME/.config/waybar/mediaplayer.py 2> /dev/null" // Script in resources folder
    },
    "custom/vpn": {
        "interval": 2,
        "return-type": "string",
        "max-length": 2,
        "min-length": 2,
        "format": "{}",
        "escape": true,
        "exec": "$HOME/.config/waybar/vpn 2> /dev/null",
        "on-click": "myterm -p -- bash -c '$HOME/bin/vpn status |less'",
        "on-click-right": "vpn toggle"
    },
    "custom/weather": {
        "interval": 60,
        "return-type": "string",
        "min-length": 32,
        "max-length": 45,
        "format": "{}",
        "escape": true,
        "exec": "$HOME/.config/waybar/weather 2> /dev/null",
        "on-click": "myterm -p -- bash -c '$HOME/.config/waybar/weather raw |less'"
    },
    "custom/btc": {
        "interval": 60,
        "return-type": "string",
        "min-length": 9,
        "max-length": 9,
        "format": "{}",
        "escape": true,
        "exec": "$HOME/.config/waybar/btc 2> /dev/null",
        "on-click": "xdg-open https://www.binance.com/en-AU/trade/ETH_AUD",
        "on-click-right": "myterm -p -- bash -c 'VERBOSE=true $HOME/.config/waybar/btc |less'"
    },
    "custom/apps_menu": {
        "interval": "once",
        "format": "𝍢",
        "on-click": "xfce4-appfinder"
    },
    "disk": {
         "interval": 30,
         "format": "{percentage_used}% {path}",
         "states": {
             "warning": 80,
             "critical": 90
         },
         "on-click": "gnome-disks"
    },
    "disk#sda": {
         "interval": 30,
         "path": "/mnt/home",
         "format": "{percentage_used}% sda",
         "on-click": "gnome-disks"
    }
}
