# sway config file

# issues:
# i3-move-resize.py
# i3-dmenu-desktop
# xfce4-power / apps?
# nm-applet - works with waybar but not with i3blocks

# Font for window titles. Will also be used by the bar unless a different font
# is used in the bar {} block below.
font pango:monospace 8

# This font is widely installed, provides lots of unicode glyphs, right-to-left
# text rendering and scalability on retina/hidpi displays (thanks to pango).
#font pango:DejaVu Sans Mono 8

# Before i3 v4.8, we used to recommend this one as the default:
# font -misc-fixed-medium-r-normal--13-120-75-75-C-70-iso10646-1
# The font above is very space-efficient, that is, it looks good, sharp and
# clear in small sizes. However, its unicode glyph coverage is limited, the old
# X core fonts rendering does not support right-to-left and this being a bitmap
# font, it doesn’t scale on retina/hidpi displays.

# Use Mouse+Mod1 to drag floating windows to their wanted position
floating_modifier Mod1
set $floatWidth 750
set $floatHeight 900
set $a Mod1
set $mod Mod4
set $c Control
set $s Shift
set $p $HOME/bin
set $menu myrofi -show run
set $screenshot $HOME/dump.png
set $dropdown "myterm --dropdown"

workspace_auto_back_and_forth yes

# start a terminal
# bindsym $mod+Return exec i3-sensible-terminal
bindsym    $a+$c+Return exec xcheck myterm
bindsym     $mod+Return exec sway-focus --toggle-scratchpad --send-to-origin floating-kitty $dropdown
bindsym $a+$c+$s+Return exec xcheck myterm
bindsym  $mod+$s+Return exec xcheck myterm

# kill focused window
# bindsym $mod+$s+q kill

# start dmenu (a program launcher)
# bindsym $mod+d exec dmenu_run
# There also is the (new) i3-dmenu-desktop which only displays applications
# shipping a .desktop file. It is a wrapper around dmenu, so you need that
# installed.
# bindsym $mod+d exec --no-startup-id i3-dmenu-desktop

bindsym $a+$c+Left  focus left
bindsym  $mod+Left  focus left
bindsym $a+$c+Right focus right
bindsym  $mod+Right focus right
bindsym $a+$c+Up    focus up
bindsym  $mod+Up    focus up
bindsym $a+$c+Down  focus down
bindsym  $mod+Down  focus down

bindsym $a+$c+$s+Left  move left 100 px
bindsym  $mod+$s+Left  move left 100 px
bindsym $a+$c+$s+Down  move down 100 px
bindsym  $mod+$s+Down  move down 100 px
bindsym $a+$c+$s+Up    move up 100 px
bindsym  $mod+$s+Up    move up 100 px
bindsym $a+$c+$s+Right move right 100 px
bindsym  $mod+$s+Right move right 100 px

bindsym $a+$c+$s+Home  move container to workspace prev_on_output; workspace prev
bindsym  $mod+$s+Home  move container to workspace prev_on_output; workspace prev
bindsym $a+$c+$s+Prior move container to workspace prev_on_output; workspace prev
bindsym  $mod+$s+Prior move container to workspace prev_on_output; workspace prev
bindsym $a+$c+$s+Next  move container to workspace next_on_output; workspace next
bindsym  $mod+$s+Next  move container to workspace next_on_output; workspace next
bindsym $a+$c+$s+End   move container to workspace next_on_output; workspace next
bindsym  $mod+$s+End   move container to workspace next_on_output; workspace next

# change container layout (stacked, tabbed, toggle split)
bindsym $mod+$c+s layout stacking
bindsym $mod+$c+w layout tabbed
bindsym $mod+$c+e layout toggle split

# toggle tiling / floating
bindsym $mod+space floating toggle
bindsym --whole-window $a+button2 floating toggle

# change focus between tiling / floating windows
bindsym $mod+$s+space focus mode_toggle

# focus the parent container
bindsym $a+$c+Insert focus parent
bindsym       $mod+Insert focus parent

# firefox (et al) are not able to map Shift-Insert to "paste PRIMARY" so do this globally:
#bindsym $s+Insert exec bash -c "wl-paste --no-newline --primary | ( sleep 0.2; sudo ydotool type --file - )"

# focus the child container
bindsym $a+$c+Delete focus child
bindsym       $mod+Delete focus child

# switch to workspace
#bindsym $a+$c+F1  workspace 1
bindsym  $mod+F1  workspace number 1
#bindsym $a+$c+F2  workspace 2
bindsym  $mod+F2  workspace number 2
#bindsym $a+$c+F3  workspace 3
bindsym  $mod+F3  workspace number 3
#bindsym $a+$c+F4  workspace 4
bindsym  $mod+F4  workspace number 4
#bindsym $a+$c+F5  workspace 5
bindsym  $mod+F5  workspace number 5
#bindsym $a+$c+F6  workspace 6
bindsym  $mod+F6  workspace number 6
#bindsym $a+$c+F7  workspace 7
bindsym  $mod+F7  workspace number 7
#bindsym $a+$c+F8  workspace 8
bindsym  $mod+F8  workspace number 8
#bindsym $a+$c+F9  workspace 9
bindsym  $mod+F9  workspace number 9
#bindsym $a+$c+F10 workspace 10
bindsym  $mod+F10 workspace number 10

bindsym $a+Tab exec xcheck sway-select-window

bindsym  $mod+Prior workspace next
bindsym  $mod+Next  workspace prev

# move focused container to workspace
bindsym $a+$c+$s+F1  move container to workspace number 1;  workspace number 1
bindsym  $mod+$s+F1  move container to workspace number 1;  workspace number 1
bindsym $a+$c+$s+F2  move container to workspace number 2;  workspace number 2
bindsym  $mod+$s+F2  move container to workspace number 2;  workspace number 2
bindsym $a+$c+$s+F3  move container to workspace number 3;  workspace number 3
bindsym  $mod+$s+F3  move container to workspace number 3;  workspace number 3
bindsym $a+$c+$s+F4  move container to workspace number 4;  workspace number 4
bindsym  $mod+$s+F4  move container to workspace number 4;  workspace number 4
bindsym $a+$c+$s+F5  move container to workspace number 5;  workspace number 5
bindsym  $mod+$s+F5  move container to workspace number 5;  workspace number 5
bindsym $a+$c+$s+F6  move container to workspace number 6;  workspace number 6
bindsym  $mod+$s+F6  move container to workspace number 6;  workspace number 6
bindsym $a+$c+$s+F7  move container to workspace number 7;  workspace number 7
bindsym  $mod+$s+F7  move container to workspace number 7;  workspace number 7
bindsym $a+$c+$s+F8  move container to workspace number 8;  workspace number 8
bindsym  $mod+$s+F8  move container to workspace number 8;  workspace number 8
bindsym $a+$c+$s+F9  move container to workspace number 9;  workspace number 9
bindsym  $mod+$s+F9  move container to workspace number 9;  workspace number 9
bindsym $a+$c+$s+F10 move container to workspace number 10; workspace number 10
bindsym  $mod+$s+F10 move container to workspace number 10; workspace number 10

# exit sway
bindsym $mod+$s+e exec "swaynag -t warning -m 'You pressed the exit shortcut. Do you really want to exit sway? This will end your session.' -b 'Yes, exit sway' 'swaymsg exit'"

# resize window (you can also use the mouse for that)
mode "resize" {
    # These bindings trigger as soon as you enter the resize mode

    # Pressing left will shrink the window’s width.
    # Pressing right will grow the window’s width.
    # Pressing up will shrink the window’s height.
    # Pressing down will grow the window’s height.

    # same bindings, but for the arrow keys
    bindsym Left resize shrink width 10 px or 10 ppt
    bindsym Down resize grow height 10 px or 10 ppt
    bindsym Up resize shrink height 10 px or 10 ppt
    bindsym Right resize grow width 10 px or 10 ppt

    # back to normal: Enter or Escape
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

#
# Swap Mode:
#
# Swaps focused window with window in direction in order to move it
# around a layout without altering the layout.

mode "swap" {
    bindsym Left mark --add "_swap", focus left, swap container with mark "_swap", focus left, unmark "_swap"
    bindsym Down mark --add "_swap", focus down, swap container with mark "_swap", focus down, unmark "_swap"
    bindsym Up mark --add "_swap", focus up, swap container with mark "_swap", focus up, unmark "_swap"
    bindsym Right mark --add "_swap", focus right, swap container with mark "_swap", focus right, unmark "_swap"

    # Return to default mode
    bindsym q mode "default"
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

#
# Move Mode:
#
mode "move" {
    bindsym Left  exec sway-move-to left
    bindsym Right exec sway-move-to right
    bindsym Down  exec sway-move-to bottom
    bindsym Up    exec sway-move-to top

    # these reflect the position on the keyboard:
    # w e r
    # s d f
    # x c v
    bindsym w     exec sway-move-to top-left
    bindsym e     exec sway-move-to mid-top
    bindsym r     exec sway-move-to top-right
    bindsym s     exec sway-move-to mid-left
    bindsym d     exec sway-move-to centre
    bindsym f     exec sway-move-to mid-right
    bindsym x     exec sway-move-to bottom-left
    bindsym c     exec sway-move-to mid-bottom
    bindsym v     exec sway-move-to bottom-right 

    # Return to default mode
    bindsym q mode "default"
    bindsym Return mode "default"
    bindsym Escape mode "default"
}

## Start i3bar to display a workspace bar (plus the system information i3status
## finds out, if available)
bar {
    separator_symbol "" 
# 

    colors {
        separator #ffff00
        statusline #ffffff
        background #323232
        # inactive_workspace #32323200 #32323200 #5c5c5c
        inactive_workspace #32323200 #32323200 #aaaaaa
    }
    # debug:
    #status_command 2>/tmp/i3blocks.err SCRIPT_DIR=$HOME/.config/i3blocks/i3blocks i3blocks -vvv  | tee /tmp/i3blocks.out
    #status_command SCRIPT_DIR=$HOME/.config/i3blocks/i3blocks i3blocks
    #swaybar_command waybar --style $HOME/.config/waybar/style.css

    swaybar_command waybar
    workspace_buttons yes
    #    font pango:sans 9
    #font "font awesome 5 free" 9
    font pango:DejaVu Sans Mono 9
}

bindsym --whole-window $mod+button1 workspace number 1
bindsym --whole-window $mod+button2 workspace number 2
bindsym --whole-window $mod+button3 workspace number 3

bindsym $a+$c+1 exec xcheck myautotype 1
bindsym  $mod+1 exec xcheck myautotype 1
bindsym $a+$c+2 exec xcheck myautotype 2
bindsym  $mod+2 exec xcheck myautotype 2
bindsym $a+$c+3 exec xcheck myautotype 3
bindsym  $mod+3 exec xcheck myautotype 3
bindsym $a+$c+4 exec xcheck myautotype 4
bindsym  $mod+4 exec xcheck myautotype 4
bindsym $a+$c+5 exec xcheck myautotype 5
bindsym  $mod+5 exec xcheck myautotype 5
bindsym $a+$c+6 exec xcheck myautotype 6
bindsym  $mod+6 exec xcheck myautotype 6
bindsym $a+$c+7 exec xcheck myautotype 7
bindsym  $mod+7 exec xcheck myautotype 7
bindsym $a+$c+8 exec xcheck myautotype 8
bindsym  $mod+8 exec xcheck myautotype 8
bindsym $a+$c+9 exec xcheck myautotype 9
bindsym  $mod+9 exec xcheck myautotype 9
bindsym $a+$c+0 exec xcheck myautotype 0
bindsym  $mod+0 exec xcheck myautotype 0
bindsym $s+$a+$c+0 exec xcheck myautotype --copy-to-key=index 0
bindsym  $s+$mod+0 exec xcheck myautotype --copy-to-key=index 0
bindsym  $c+$mod+0 exec xcheck myautotype --primary --copy-to-key 0
bindsym $s+$a+$c+9 exec xcheck myautotype 10
bindsym  $s+$mod+9 exec xcheck myautotype 10

for_window [class="XEyes"]                      floating enable
for_window [class="Cssh"]                       floating enable
for_window [app_id="pcmanfm"]                   floating enable
for_window [app_id="zenity"]                    floating enable
for_window [class="Gdcalc"]                     floating enable, border normal
for_window [class="Gimp"]                       floating enable, border normal
for_window [app_id="gjots2"]                    floating enable, border normal
for_window [class="MEGAsync"]                   floating enable, border normal
for_window [class="Gnome-power-statistics"]     floating enable, border normal
for_window [class="Nomacs"]                     floating enable, border normal
for_window [class="Remote-viewer"]              floating enable, border normal
for_window [class="Vncconfig"]                  floating enable, border normal, move position center
for_window [class="Vncviewer"]                  floating enable, border normal
for_window [class="XClock"]                     floating enable, border pixel 2, move position 0 0
for_window [class="XTerm"]                      floating enable, border normal, move position cursor
for_window [app_id="xfce4-appfinder"]           floating enable, border normal
for_window [class="Xsane"]                      floating enable, border normal
for_window [class="^Emacs$" instance="^Ediff$"] floating enable, border normal
for_window [app_id="emacs"]                     floating enable, border none
for_window [class="feh"]                        floating enable, border normal
for_window [app_id="imv"]                       floating enable, border normal
for_window [class="floating"]                   floating enable, border normal, move position center
for_window [title="floating"]                   floating enable, border normal, move position center
for_window [app_id="floating"]                  floating enable, border normal, move position center
for_window [app_id="floating-kitty"]            floating enable, border pixel 2, move position 0 0
for_window [window_role="Preferences"]          floating enable, border normal
for_window [window_role="bubble"]               floating enable, border normal
for_window [window_role="pop-up"]               floating enable, border normal
for_window [window_role="task_dialog"]          floating enable, border normal
for_window [window_type="dialog"]               floating enable, border normal
for_window [window_type="menu"]                 floating enable, border normal
for_window [class="XVkbd"]                      floating enable, border normal
# no WM_CLASS for xev:
for_window [title="^Event Tester$"]             floating enable, border normal
for_window [app_id="gnome-system-monitor"]      floating enable, border none
for_window [app_id="gnome-control-center"]      floating enable, border none
for_window [app_id="gnome-disks"]               floating enable, border none
for_window [app_id="blueman-manager"]           floating enable, border normal
for_window [app_id="pavucontrol"]               floating enable, border normal
for_window [class="Pulseaudio-equalizer.py"]    floating enable, border normal
for_window [class="Tk"]                         floating enable, border normal

#bindsym $a+$c+a exec xcheck pkill xclock || xclock -update 1
bindsym $a+$c+a exec pkill xclock || xclock -update 1
bindsym  $mod+a exec pkill xclock || xclock -update 1
bindsym $a+$c+b exec xcheck browser
bindsym  $mod+b exec xcheck browser

bindsym $a+$c+$s+c exec myreload
bindsym  $mod+$s+c exec myreload

# colour picker:
bindsym $mod+Shift+d exec grim -g "$(slurp -p)" -t ppm - | convert - -format '%[pixel:p{0,0}]' txt:- | tail -n 1 | cut -d ' ' -f 4 | wl-copy

# Start your launcher
bindsym $mod+d exec $menu
# already in use:
#bindsym       $mod+$s+e exec ...
bindsym $a+$c+$s+e exec xcheck sway-run-or-raise "emacs" "Emacs"
bindsym $mod+f fullscreen
# keep C-M-f for x2go/nx etc
bindsym $a+$c+g exec pkill gdcalc || gdcalc
bindsym  $mod+g exec pkill gdcalc || gdcalc
bindsym $a+$c+h split h
bindsym  $mod+h split h
bindsym $a+$c+i exec xcheck sway-run-or-raise gjots2 Gjots2 "gjots2 $HOME/general/journal/journal.gjots"
bindsym  $mod+i exec xcheck sway-run-or-raise gjots2 Gjots2 "gjots2 $HOME/general/journal/journal.gjots"
bindsym $a+$c+j exec xcheck -- bash -c "sway-run-or-raise emacs Emacs 'emacs ~/Sync/bh.org' && sleep 1 && emacsclient --no-wait ~/Sync/bh.org"
bindsym  $mod+j exec xcheck -- bash -c "sway-run-or-raise emacs Emacs 'emacs ~/Sync/bh.org'  && sleep 1 && emacsclient --no-wait ~/Sync/bh.org"
bindsym $a+$c+k border toggle
bindsym  $mod+k border toggle

bindsym    $a+$c+l   exec xcheck mylock
bindsym     $mod+l   exec xcheck mylock
bindsym $s+$a+$c+l   exec xcheck mylock at-home-mode
bindsym  $s+$mod+l   exec xcheck mylock at-home-mode
bindsym  $c+$mod+l   exec xcheck mylock safe-mode
bindsym $c+$s+$mod+l exec xcheck mylock lock-now

bindsym $a+$c+m mode "move"
bindsym  $mod+m mode "move"

bindsym    $a+$c+n exec xcheck vpn on
bindsym     $mod+n exec xcheck vpn on
bindsym $a+$c+$s+n exec xcheck vpn off
bindsym  $mod+$s+n exec xcheck vpn off
#bindsym $a+$c+n exec xcheck sway-run-or-raise gjots2 Gjots2 "gjots2 $HOME/general/journal/work-notes.gjots"
#bindsym  $mod+n exec xcheck sway-run-or-raise gjots2 Gjots2 "gjots2 $HOME/general/journal/work-notes.gjots"
# 'o' is free:
bindsym $mod+o exec swaymsg 'border pixel 3' && sleep .2 && swaymsg 'border pixel 1'
#bindsym $a+$c+o exec xcheck tile --limit-to-cons all
#bindsym $mod+o  exec xcheck tile --limit-to-cons all
bindsym $a+$c+p exec xcheck sway-run-or-raise keepassxc "KeePassXC" "keepassxc $HOME/.SpiderOakSync/general/misc/personal.kdbx"
bindsym  $mod+p exec xcheck sway-run-or-raise keepassxc "KeePassXC" "keepassxc $HOME/.SpiderOakSync/general/misc/personal.kdbx"
bindsym    $a+$c+q exec xcheck sway-run-or-raise oocalc
bindsym     $mod+q exec xcheck sway-run-or-raise oocalc
bindsym $a+$c+$s+q kill
bindsym  $mod+$s+q kill

bindsym $mod+r mode "resize"
#bindcode $mod+27 mode "resize"

# $a+$c+r is already 'resize'
#bindsym $a+$c+r exec xcheck myrofi -show run
bindsym $mod+$c+r exec xcheck myrofi -show run

# already defined: bindsym $mod+$c+s layout stacking
bindsym  $a+$c+s mode "swap"
bindsym  $mod+s  mode "swap"
bindsym  $mod+$s+s floating enable, resize set $floatWidth $floatHeight, move position center, border normal
#bindsym  $mod+$c+s floating enable, resize set $floatWidth $floatHeight, move position center, border normal

bindsym $a+$c+t exec xcheck -- bash -c "sway-run-or-raise emacs Emacs 'emacs /Sync/todo.org' && sleep 1 && emacsclient --no-wait ~/Sync/todo.org"
bindsym  $mod+t exec xcheck -- bash -c "sway-run-or-raise emacs Emacs 'emacs /Sync/todo.org' && sleep 1 && emacsclient --no-wait ~/Sync/todo.org"
bindsym $a+$c+u exec xcheck -- bash -c "sway-run-or-raise emacs Emacs 'emacs /Sync/unix.org' && sleep 1 && emacsclient --no-wait ~/Sync/unix.org"
bindsym  $mod+u exec xcheck -- bash -c "sway-run-or-raise emacs Emacs 'emacs /Sync/unix.org' && sleep 1 && emacsclient --no-wait ~/Sync/unix.org"
bindsym $a+$c+v split v
bindsym  $mod+v split v
bindsym $a+$c+w exec xcheck sway-run-or-raise oowriter
bindsym  $mod+w exec xcheck sway-run-or-raise oowriter

# x is available

bindsym $mod+y     exec myclipman pick  -t CUSTOM -T 'wofi --dmenu -p pick'
bindsym $a+$c+y    exec myclipman pick  -t CUSTOM -T 'wofi --dmenu -p pick'
bindsym $mod+$s+y  exec myclipman clear -t CUSTOM -T 'wofi --dmenu -p clear'
bindsym $a+$c+$s+y exec mrclipman clear -t CUSTOM -T 'wofi --dmenu -p clear'

bindsym $a+$c+z exec xcheck sway-run-or-raise "veracrypt"
bindsym  $mod+z exec xcheck sway-run-or-raise "veracrypt"

bindsym  $mod+question exec xcheck sway-prop
bindsym $a+$c+question exec xcheck sway-prop

# fits all visible floaters:
bindsym    $mod+backslash   exec sway-fit-floats               --x-origin=20 --y-origin=20 --padx=20 --pady=20 -v
# if no floaters, bring them from scratchpad; else send to scratchpad:
bindsym $mod+$c+backslash   exec sway-fit-floats --toggle      --x-origin=20 --y-origin=20 --padx=20 --pady=20 -v
# send visible floating windows to scratchpad:
bindsym $mod+$s+backslash   exec sway-fit-floats -S --no-focus --x-origin=20 --y-origin=20 --padx=20 --pady=20 -v

bindsym       Print exec xcheck myscreendump
bindsym    $c+Print exec xcheck myscreendump root
bindsym    $s+Print exec xcheck myscreendump window
bindsym $c+$s+Print exec xcheck myscreendump rect
bindsym  $mod+Print exec xcheck myscreendump frame

bindsym --release $a+$c+Escape exec xcheck sway-kill
bindsym --release  $mod+Escape exec xcheck sway-kill

# these are for achar for single handed use:
bindsym $a+F11                        exec xcheck toggle-master down
bindsym $a+F12                        exec xcheck toggle-master up

# use --locked so the keys can be accessed while sway is locked!!
bindsym --locked XF86AudioLowerVolume          exec xcheck toggle-master down
bindsym --locked XF86AudioRaiseVolume          exec xcheck toggle-master up
bindsym --locked XF86AudioMute                 exec xcheck toggle-master
bindsym --locked XF86AudioPlay                 exec xcheck mplayer-cmd pause
bindsym --locked XF86AudioPrev                 exec xcheck mplayer-cmd prev
bindsym --locked XF86AudioNext                 exec xcheck mplayer-cmd next

bindsym --locked XF86MonBrightnessUp   exec brightness higher
bindsym --locked XF86MonBrightnessDown exec brightness lower

#bindsym XF86RotateWindows            exec xcheck (date >/tmp/lid-date)
bindsym XF86TouchpadToggle            exec xcheck mytoggletouchpad
bindsym XF86Calculator exec xcheck pkill gdcalc || gdcalc

# Make the currently focused window a scratchpad
bindsym $a+$c+minus scratchpad show
bindsym  $mod+minus scratchpad show
# Show the first scratchpad window
bindsym $a+$c+$s+minus move scratchpad
bindsym  $mod+$s+minus move scratchpad

bindsym Menu        exec xcheck myrofi -show run
bindsym $mod+grave [con_mark=_prev] focus

# i3: new_window pixel 1
default_border pixel 1
#i3: new_float normal
default_floating_border normal

popup_during_fullscreen smart

focus_follows_mouse always

# Notes on quoting in here:
# " inside a ".." needs \\
# $ inside a ".." needs \

input "2:7:SynPS/2_Synaptics_TouchPad" {
    tap enabled
    scroll_method edge
    scroll_factor 0.25
}

# CapsLock to hyper:
input * {
    # xkb_options caps:hyper
    xkb_options caps:ctrl_modifier
}

exec mako
exec sway-prep-xwayland
exec mylock safe-mode
exec_always bash -c "killall sway-track-prev-focus; exec sway-track-prev-focus"
exec_always bash -c "killall sway-track-firefox; exec sway-track-firefox"
exec_always start-powerd
exec_always sudo systemctl restart ydotool
exec wlr-sunclock -a tblr -l bottom
exec /usr/libexec/gsd-xsettings
# use --no-persist otherwise insecure - also \n are lost:
exec xcheck -- wl-paste -t text --watch myclipman
# needed for blueman-manager in f34:
exec blueman-applet
exec nm-applet --indicator

# /usr/libexec/polkit-gnome-authentication-agent-1 fails eg with firewall-config:
exec lxpolkit

workspace number 3
exec mythfrontend
workspace number 2
exec firefox
workspace number 1
exec kitty
exec emacs
exec $dropdown; exec swaymsg 'move window to scratchpad'
# f43 starts dbus for us at login, even at the console: this allows ssh-add (or is it g-k-d?) to popup a window
exec dbus-update-activation-environment --systemd DISPLAY

# this approach does not work because mythfrontend is always fullscreen.
# what we really need is that all apps support idle inhibit when playing media
# until then, we can't use swayidle!!
# firefox, mpv, kodi all implement inhibit timer
# vlc, mythtv do not
# mplayer is flakey (at least) on sway
#for_window [app_id="mythfrontend"] inhibit_idle fullscreen
#for_window [class="vlc"] inhibit_idle fullscreen
#exec swayidle -w timeout 600 "swaymsg 'output * dpms off'" resume "swaymsg 'output * dpms on'"

# ??? exec gnome-keyring-daemon -r -d --components=secrets

# use wlr-sunclock:
# output * bg /usr/share/backgrounds/sway/Sway_Wallpaper_Blue_1920x1080.png fill

# press $mod+XF86AudioMute to pass all keys to the underlying application (wayvnc vncviewer ...)
mode passthrough {
	bindsym $mod+XF86AudioMute mode default
}
bindsym $mod+XF86AudioMute mode passthrough

#  Local Variables:
#  eval: (add-hook (make-local-variable 'after-save-hook) 'bh:ensure-in-vc-or-check-in t)
#  End:
