;; minimal file to hold 'custom' stuff and to daisy-chain to config.el
;; - mainly so that the eval's at the end of config.el don't trigger
;; the confirmation

;; see also M-x emacs-init-time
(defconst emacs-start-time (current-time))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(bmkp-last-as-first-bookmark-file "/home/bhepple/.emacs.bmk")
 '(company-quickhelp-color-background "#4F4F4F")
 '(company-quickhelp-color-foreground "#DCDCCC")
 '(compilation-message-face 'default)
 '(cua-global-mark-cursor-color "#2aa198")
 '(cua-normal-cursor-color "#657b83")
 '(cua-overwrite-cursor-color "#b58900")
 '(cua-read-only-cursor-color "#859900")
 '(custom-safe-themes
   '("aa85228e4b6d188c3c0b8553548142c2c75726429614376c26255a51378982f5" "bde7af9e749d26cbcbc3e3ac2ac9b13d52aa69b6148a8d6e5117f112f2797b42" "a412e9e5220440ddd13aa2c18c0aa3fce4012336bb92a672ec8d5b78a997c461" "e6df46d5085fde0ad56a46ef69ebb388193080cc9819e2d6024c9c6e27388ba9" "9df966126d82dd8a4cc237c538738448f5258ae4c5674931fbfa63cfb9a21049" "e2b089bae067e904bc5ebc8a34549c380d821a8d9a796a7112df358eb5f3b323" "577d83cd3aa452aa2b9b2c0ffc1c83eca92cdec16335d7f6a6623e5137e20b00" "3cd506a60ad2c638ddc3c7be9818b4d9666a2e5958d3cb0f144b3ff96c4e2b51" "c2ce0c44b6aec14416bd54f992a046b03d75bd8596407110dbe530654a9f65ca" "07f5f0c622f66d6d5f2e8a858bed8e4fe1fc92316cfec624d8ac277458a5896f" "f39b204d431cd83cd91aeac50330352b50d0167612fc103a1f5acb63f1eb9e0d" "7786d07a5e5a5627bee60e2dffd18eb8e604a8a57d4be23efa1bed5b8dc2f21c" default))
 '(fci-rule-color "#eee8d5")
 '(highlight-changes-colors '("#d33682" "#6c71c4"))
 '(highlight-symbol-colors
   '("#efe5da4aafb2" "#cfc5e1add08c" "#fe53c9e7b34f" "#dbb6d3c3dcf4" "#e183dee1b053" "#f944cc6dae48" "#d360dac5e06a"))
 '(highlight-symbol-foreground-color "#586e75")
 '(hl-bg-colors
   '("#e1af4b" "#fb7640" "#ff6849" "#ff699e" "#8d85e7" "#74adf5" "#6ccec0" "#b3c34d"))
 '(hl-fg-colors
   '("#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3" "#fdf6e3"))
 '(hl-paren-colors '("#2aa198" "#b58900" "#268bd2" "#6c71c4" "#859900"))
 '(imenu-auto-rescan t)
 '(imenu-max-items 500)
 '(imenu-sort-function nil)
 '(jdee-db-active-breakpoint-face-colors (cons "#0F1019" "#D85F00"))
 '(jdee-db-requested-breakpoint-face-colors (cons "#0F1019" "#79D836"))
 '(jdee-db-spec-breakpoint-face-colors (cons "#0F1019" "#767676"))
 '(lsp-ui-doc-border "#586e75")
 '(nrepl-message-colors
   '("#dc322f" "#cb4b16" "#b58900" "#5b7300" "#b3c34d" "#0061a8" "#2aa198" "#d33682" "#6c71c4"))
 '(objed-cursor-color "#D83441")
 '(package-selected-packages
   '(neotree elfeed-dashboard elfeed modus-themes doom-modeline zenburn-theme json-navigator rpm-spec-mode all-the-icons esup dumb-jump json-mode magit outline-magic outline-cycle discover-my-major which-key helpful flx-ido ido-vertical-mode ido-completing-read+ use-package unfill smex ripgrep key-chord ibuffer-vc))
 '(pos-tip-background-color "#eee8d5")
 '(pos-tip-foreground-color "#586e75")
 '(rustic-ansi-faces
   ["#0D0E16" "#D83441" "#79D836" "#D8B941" "#3679D8" "#8041D8" "#36D8BD" "#CEDBE5"])
 '(safe-local-variable-values
   '((eval setq python-indent 4)
     (eval setq tab-width 4)
     (eval setq-default indent-tabs-mode nil)
     (eval setq compile-command "./gjots2 test.gjots")
     (eval outline-hide-sublevels 4)
     (eval add-hook
           (make-local-variable 'after-save-hook)
           'bh:ensure-in-vc-or-check-in t)))
 '(smartrep-mode-line-active-bg (solarized-color-blend "#859900" "#eee8d5" 0.2))
 '(term-default-bg-color "#fdf6e3")
 '(term-default-fg-color "#657b83")
 '(weechat-color-list
   '(unspecified "#fdf6e3" "#eee8d5" "#a7020a" "#dc322f" "#5b7300" "#859900" "#866300" "#b58900" "#0061a8" "#268bd2" "#a00559" "#d33682" "#007d76" "#2aa198" "#657b83" "#839496")))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )

(setq dotfiles-dir "~/.emacs.d/")
(load-file (concat dotfiles-dir "config.el"))

;;; Finalization

(let ((elapsed (float-time (time-subtract (current-time)
                                          emacs-start-time))))
  (message "Loading %s...done (%.3fs)" load-file-name elapsed))

(add-hook 'after-init-hook
          `(lambda ()
             (let ((elapsed
                    (float-time
                     (time-subtract (current-time) emacs-start-time))))
               (message "Loading %s...done (%.3fs) [after-init]"
                        ,load-file-name elapsed))) t)
