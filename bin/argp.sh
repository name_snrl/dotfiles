#!/usr/bin/env bash
# -*- shell-script -*-

# Copyright 2008-2021 Bob Hepple <bob.hepple@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# http://sourceforge.net/projects/argpsh
# http://bhepple.freeshell.org/oddmuse/wiki.cgi/process-getopt

# $Id: argp.sh,v 1.14 2012/08/19 04:22:16 bhepple Exp $

# Certainly would not work on plain old sh, csh, ksh ... .

ARGP_argp_sh_usage() {
    cat <<"EOF"
Usage: argp.sh or argp

A wrapper for getopt(1) which simulates argp(3) for bash. Normally
other scripts pipe their option descriptions in here for automatic
help and man-page production and for generating the code for
getopt(1). Requires bash-3+. See also argp(1) which is a binary (and
much faster) version of this.

See http://sourceforge.net/projects/argpsh
See http://bhepple.freeshell.org/oddmuse/wiki.cgi/argp.sh

It buys you:

o all the goodness of getopt
o define options in one central place together with descriptions using a
  simple flat file format or XML.
o fewer 'magic' and duplicated values in your code
o automatic consistency between the getopt calling parameters, the case
  statement processing the user's options and the help/man pages
o less spaghetti in your own code
o easier to maintain
o help-page and man-page printing - all from the same data sources
o checking of option consistency at runtime
o range checking of option values at runtime
o pretty easy to use
o portable to OS's without long option support - the help page
  adapts too

###############################################################################

Usage: argp.sh / argp reads its configuration from stdin (fd0) (see
below for details) and outputs the following:

* --help output on stdout fd1
* errors on stderr fd2
* a list of commands on fd3

The calling program can then 'eval' that list of commands in order to
set environment variables as set by the user in command line options.
Also, any remaining arguments after the options are removed are set to
$1 $2 ...

eg. if the user typed 'myprog --foobar --barfoo=three a b c' then after
'eval'ing the fd3 output of argp.sh / argp you would have
FOOBAR="set"
BARFOO="three"
$1 $2 ... = a b c

Usually, the calling program would invoke argp.sh / argp like this:

    exec 4>&1
    eval "$(echo "$ARGS" | argp.sh "$@" 3>&1 1>&4 || echo exit $? )"
    exec 4>&-

Here's a bit of an explanation in case you're interested:
    exec 4>&1  # make fd4 a copy of fd1 stdout eg /dev/pts/0, the terminal
    eval $(
        # inside this sub-shell fd1 is now a pipe
        echo "$ARGS" |

        # 3>&1: connect fd3 to fd1 (the pipe) so that anything printed
        # on fd3 is fed to the pipe (and eventually eval'd).
        #
        # 1>&4: fd1 is connected to fd4 ie /dev/pts/0 so that anything
        # that argp.sh / argp prints on stdout (eg the --help output) ends up
        # on the terminal:

        argp.sh "$@" 3>&1 1>&4 || echo exit $?
    )
    exec 4>&- # close fd4

###############################################################################

It's easier to look at a running script but here's the manual:

###############################################################################
# this is a comment.

# The following line defines our program name. We might also write
# this as ARGP_PROG=$(basename $0):
ARGP_PROG=my-script

# '--quiet' is a built-in option, but we normally delete it if 'verbose' is
# supported.
ARGP_DELETE=quiet

# this defines the program's version string to be displayed by the -V option:
ARGP_VERSION=1.6

# this string is added as a prefix to all option names on output, eg
if you need to use an option that clashes with a system environment
parameter eg --home:
ARGP_PREFIX=FOO_

# if this is set then multiple invocations of a string or array option will
# be concatenated using this separator eg '-s s -s b' will give 'a:b'
ARGP_OPTION_SEP=:

# Here are the options that this program will accept:
########       ##### ###     #### #####   ###########
# name         sname arg     type range   description
########       ##### ###     #### #####   ###########

name=default : 'name' must be present and unique. 'default' must be given but
               may be ''

sname        : the single-letter short name of the option (use '' if a short
               name is not needed)

arg          : the name of the argument to be used in --help (not for
               booleans)

type         : b (boolean), i (integer), d (double), s[:] (string),
               a[:] (array) or u (url). Default is boolean if arg is
               not given, otherwise string. If a boolean is
               initialised to '0', then it is incremented every time
               it is given. If a boolean is initialised to anything
               else then it flips between 'default' and 'range' every
               time it is given. If 's:' or 'a:' then ':' overrides
               ARGP_OPTION_SEP for this option. Any syntactically
               clean string can be used instead of ':' ie not ' or ".

range        : for numeric options: min:max eg 1.2:4.5 or min-max eg 1-3
               for string options: an extended regexp
               for array options: a space separated list of alternates
               in quotes "..." or '...'
               for boolean options: the value to assign the option when set

desc         : leave empty for hidden options. Long descriptions can
               span lines by putting '\' at the end of the line and by
               terminated the description with a '.' in the first
               column.

###############################################################################
Examples:

# This is the simplest possible option definition. It specifies a
# hidden option (--hidden) which does not appear in the help or man
# pages. It defaults to being a simple flag (boolean) with a default
# value of '' and 'set' if --hidden is on the command line.
HIDDEN=

# a boolean option with a short name and a numeric default which is
# incremented every time the --bool, -b option is given:
BOOL='0'       b     ''      b    ''      description of this option

# this is a (boolean) flag which gets set to the string 'foobar' when
# --flag, -f is on the command line otherwise it is set to 'barfoo':
FLAG='barfoo' f    ''      b    'foobar'  a flag

# here is an integer value option which must sit in a given range:
INT=1         i     units   i    '1:3'  an integer.

# here is an array value ie just a string which must take one of
# the values in the 'range':
ARRAY=a        a     widgets a    'a b'  an array

# this option is a simple string which will be checked against a regex(7):
STRING=''      s     string  s    '^foo.*bar$|^$'  a string.

# a double value which is checked against a range:
DOUBLE=1       d     miles   d    0.1:1.3 a double.

# this is a URL which will be checked against the URL regex and has a
# default value
URL='http://www.foobar.com'     u     url     u    ''      a url.

# delete this one as we want to use the 'q' option for something else:
ARGP_DELETE=quiet

# this uses the same short letter 'q' as the --quiet option which was
# deleted above
QUAINT=''      q     ''      s    ''      a quaint description

# here we define the non-option arguments that the command takes:
ARGP_ARGS= [--] [args]
ARGP_SHORT=This is a short description for the first line of the man page.
ARGP_USAGE=
This is a longer description.
Spring the flangen dump. Spring the flingen dump. Spring the flangen
dump. Spring the flingen dump.

###############################################################################

XML can also be instead of the above (provided xmlstarlet is available):

<?xml version="1.0" encoding="UTF-8"?>
<argp>
  <prog>fs</prog>
  <args>[--] [pattern]</args>
  <short>Search for filenames in sub-directories.</short>
  <delete>quiet</delete>
  <version>1.2</version>
  <prefix>FOO_</prefix>
  <usage>Presently this is just a shorthand for:

find . -follow \$EXCLUDE -type $TYPE -name '*pattern*' -print 2>/dev/null |sort
  </usage>
  <option name="EXCLUDE" sname="x" type="s" arg="directory">exclude directory</option>
  <option name="DIR"     sname="d" type="b" default="f" range="d">search for a directory rather than a file</option>
  <option name="SECRET" type="b"/>
</argp>

Note that the attribute 'name' is required - the others are all
optional. Also the option value should be on a single line (the
program usage can be on multiple lines).

###############################################################################

Note that --verbose, --help, --quiet and --version options will be
added automatically. Also, a hidden option '--print-man-page' is
provided to print a skeleton man(1) page which can be used as the
starting point for a full man page. Another hidden option
'--print-xml' prints out the XML equivalent of the argp input.

If POSIXLY_CORRECT is set, then option parsing will end on the first
non-option argument (eg like ssh(1)).

###############################################################################

Here is a sample of the output when the command is called with --help:

Usage: my-script [OPTION...] [--] [args]
This is a longer description. Spring the flangen dump. Spring the
flingen dump. Spring the flangen dump. Spring the flingen dump."

Options:

  -a, --array=<widgets>      an array Must be of type 'a'. Must be in the range
                             'a b'.
  -b, --bool                 description of this option
  -d, --double=<miles>       a double. Must be of type 'd'. Must be in the
                             range '0.1-1.3'.
  -f, --flag                 a flag
  -i, --int=<units>          an integer. Must be of type 'i'. Must be in the
                             range '1-3'.
  -q, --quaint               a quaint description Must fit the regex ''.
  -s, --string=<string>      a string. Must fit the regex '^foo.*bar$|^$'.
  -u, --url=<url>            a url. Must fit the regex
'^(nfs|http|https|ftp|file)://[[:alnum:]_.-]*[^[:space:]]*$'.
  -v, --verbose              be verbose
  -h, --help                 print this help message
  -V, --version              print version and exit

EOF
}

argp_sh_version() {
    echo "$GARGP_VERSION"
}

print_array() {
    let n=0
    for i in "$@"; do printf %s " [$n]='$i'"; let n+=1; done
}

debug_args() {
    {
        local i
        printf %s "${FUNCNAME[1]} "
        print_array "$@"
        echo
    } >&2
}

abend() {
    STAT=$1; shift
    echo "$ARGP_PROG: $*" | fmt -w "$GARGP_RMARGIN" >&2
    echo "exit $STAT;" >&3
    exit "$STAT"
}

# first param (name) can be an env or an option name
add_opt() {
    local NAME DEFAULT DESC SOPT ARG LOPT TYPE RANGE

    NAME=$(convert_to_env_name "$1")
    LOPT=$(convert_to_option_name "$1") # long name of the option
    DEFAULT=${2:-}
    SOPT="${3:-}" # short option letter - optional
    ARG="${4:-}" # argument label - optional
    TYPE="${5:-}" # type of the argument - optional
    RANGE="${6:-}" # range for the argument - optional
    DESC="${7:-}" # essential except for 'silent/secret' options
    local ALLOWED_CHARS='^[a-zA-Z0-9_][a-zA-Z0-9_]*$'
    local OPT

    [[ "$ARGP_DEBUG" ]] && debug_args "$@"
    [[ "$NAME" ]] || abend 1 "$ARGP_PROG: argp.sh: add_opt requires a name"

    [[ "$NAME" =~ $ALLOWED_CHARS ]] || {
        abend 1 "argp.sh: apt_opt: NAME (\"$NAME\") must obey the regexp $ALLOWED_CHARS"
    }

    export ARGP_ARG_"$NAME"="$ARG"
    export ARGP_DEFAULT_"$NAME"="$DEFAULT"
    export "$NAME"="$DEFAULT"

    # check it's not already in use
    for OPT in ${ARGP_OPTION_LIST:-}; do
        if [[ "$NAME" == "$OPT" ]]; then
            abend 1 "$ARGP_PROG: argp.sh: ${FUNCNAME[0]}: option name \"$NAME\" is already in use"
        fi
        # check that the (short) option letter is not already in use:
        if [[ "$SOPT" ]]; then
            if [[ "$SOPT" == "$(get_opt_letter "$OPT")" ]]; then
                abend 1 "$ARGP_PROG: argp.sh: ${FUNCNAME[0]}: short option \"$SOPT\" is already in use by $OPT"
            fi
        fi
    done

    if [[ "$SOPT" ]]; then
        [[ ${#SOPT} -ne 1 ]] && {
            abend 1 "$ARGP_PROG: argp.sh: ${FUNCNAME[0]}: short option \"$SOPT\" for option $NAME is not a single character"
        }
        export ARGP_SOPT_"$NAME"="$SOPT"
    fi
    export ARGP_LOPT_"$NAME"="$LOPT"

    if [[ "$DESC" ]]; then
        export ARGP_DESC_OPT_"$NAME"="$DESC"
    fi

    # shellcheck disable=SC2018
    # shellcheck disable=SC2019
    TYPE=$( echo "$TYPE"| tr 'A-Z' 'a-z' )
    # use a while loop just for the 'break':
    while true; do
        case "$TYPE" in
            b)
                break
                ;;
            i)
                [[ "$RANGE" ]] || break
                echo "$RANGE" | egrep -q "$GARGP_INT_RANGE_REGEX" && break
                ;;
            r|f|d)
                [[ "$RANGE" ]] || break
                echo "$RANGE" | egrep -q "$GARGP_FLOAT_RANGE_REGEX" && break
                ;;
            s*)
                [[ "$RANGE" ]] || break
                # just test the regex:
                echo "" | egrep -q "$RANGE"
                [[ $? -eq 2 ]] || break
                ;;
            a*)
                [[ "$RANGE" ]] && break
                ;;
            u*)
                local SEP=${TYPE:1:1}
                TYPE=s$SEP
                RANGE="$GARGP_URL_REGEX"
                break
                ;;
            "")
                TYPE=s
                break
                ;;
        esac
        abend 1 "$ARGP_PROG: argp.sh: ${FUNCNAME[0]}: bad argument type ('$TYPE') or range ('$RANGE') for option '$NAME'."
    done
    export ARGP_TYPE_"$NAME"="$TYPE"
    export ARGP_RANGE_"$NAME"="$RANGE"
    export ARGP_WAS_SET_"$NAME"=""

    ARGP_OPTION_LIST="${ARGP_OPTION_LIST:-} $NAME"
}

get_opt_letter() {
    [[ "$ARGP_DEBUG" ]] && debug_args "$@"
    local NAME="$1"
    local L="ARGP_SOPT_$NAME"
    printf %s  "${!L:-}"
    [[ "$ARGP_DEBUG" ]] && echo "returning ${!L:-}" >&2
}

get_opt_string() {
    [[ "$ARGP_DEBUG" ]] && debug_args "$@"
    local NAME="$1"
    local L="ARGP_LOPT_$NAME"
    printf %s  "${!L:-}"
}

get_opt_arg() {
    [[ "$ARGP_DEBUG" ]] && debug_args "$@"
    local NAME="$1"
    local L="ARGP_ARG_$NAME"
    printf %s "${!L:-}"
}

# convert an environment name (eg ARGP_DEBUG) to a long option name (eg argp-debug)
convert_to_option_name() {
    echo "$1" | tr '[:upper:]_' '[:lower:]-'
}

convert_to_env_name() {
    echo "$1" | tr '[:lower:]-' '[:upper:]_'
}

get_opt_type() {
    [[ "$ARGP_DEBUG" ]] && debug_args "$@"
    local NAME="$1"
    local T="ARGP_TYPE_$NAME"

    printf %s "${!T:-}"
}

get_opt_range() {
    [[ "$ARGP_DEBUG" ]] && debug_args "$@"
    local NAME="$1"
    local R="ARGP_RANGE_$NAME"

    printf %s "${!R:-}"
}

get_opt_was_set() {
    [[ "$ARGP_DEBUG" ]] && debug_args "$@"
    local NAME="$1"
    local R="ARGP_WAS_SET_$NAME"

    printf %s "${!R:-}"
}

get_opt_default() {
    [[ "$ARGP_DEBUG" ]] && debug_args "$@"
    local NAME="$1"
    local D=ARGP_DEFAULT_$NAME
    local DEFAULT="${!D}"
    printf %s "${DEFAULT:-}"
}

get_opt_raw_desc() {
    [[ "$ARGP_DEBUG" ]] && debug_args "$@"
    local NAME="$1"
    local L="ARGP_DESC_OPT_$NAME"
    printf %s "${!L:-}"
}

get_opt_desc() {
    [[ "$ARGP_DEBUG" ]] && debug_args "$@"
    local NAME
    NAME="$1"
    local L
    L=$( get_opt_raw_desc "$NAME" )
    [[ -z "$L" ]] && return 0 # hidden option

    printf %s "$L"
    local TYPE
    TYPE=$(get_opt_type "$NAME")
    local RANGE
    RANGE=$(get_opt_range "$NAME")

    DEFAULT=$(get_opt_default "$NAME")
    [[ "$DEFAULT" ]] && echo -n " Default is '$DEFAULT'."

    if  [[ "$TYPE" && "$TYPE" != "b" && "$TYPE" != "h" ]]; then
        if [[ "$TYPE" != s* || "$RANGE" ]]; then
            if [[ "$TYPE" != s* ]]; then
                echo -n " Must be of type '$TYPE'."
            fi
            if [[ "$RANGE" ]]; then
                case "$TYPE" in
                    s*)
                        echo -n " Must fit the regex '$RANGE'."
                        ;;
                    *)
                        echo -n " Must be in the range '$RANGE'."
                        ;;
                esac
            fi
        fi
    fi
}

# allow them to specify the long option name or the environment parameter
del_opt() {
    [[ "$ARGP_DEBUG" ]] && debug_args "$@"

    local OPT_NAME PRE ENV_NAME
    for OPT_NAME in $@; do
        ENV_NAME=$(convert_to_env_name "$OPT_NAME")
        OPT_NAME=$(convert_to_option_name "$OPT_NAME")
        for PRE in ARGP_SOPT_ ARGP_LOPT_ ARGP_DESC_OPT_ ARGP_ARG_  ARGP_TYPE_ ARGP_RANGE_ ARGP_WAS_SET_; do
            local N=$PRE$ENV_NAME
            [[ "${!N:-}" ]] && unset "$PRE$ENV_NAME"
        done
        local OPT_LIST OPT
        OPT_LIST=""
        for OPT in ${ARGP_OPTION_LIST:-}; do
            [[ "$OPT" == "$ENV_NAME" ]] || OPT_LIST="$OPT_LIST $OPT"
        done
        ARGP_OPTION_LIST="$OPT_LIST"

        [[ "$OPT_NAME" == "$GARGP_HELP_loption" ]] && {
            GARGP_HELP_option=
            GARGP_HELP_loption=
        }
        [[ "$OPT_NAME" == "$GARGP_VERSION_loption" ]] && {
            GARGP_VERSION_option=
            GARGP_VERSION_loption=
        }
        [[ "$OPT_NAME" == "$GARGP_PRINTMAN_loption" ]] && {
            GARGP_PRINTMAN_loption=
        }
        [[ "$OPT_NAME" == "$GARGP_PRINTXML_loption" ]] && {
            GARGP_PRINTXML_loption=
        }
    done
}

# prints only short options that take no parameter
print_short_flags() {
    local NAME DESC L A FLAGS=""

    for NAME in ${ARGP_OPTION_LIST:-}; do
        [[ "$NAME" == $( convert_to_env_name "$GARGP_ENDOPTS_loption") ]] && continue
        DESC=$(get_opt_desc "$NAME")
        [[ "$DESC" ]] || continue
        L=$(get_opt_letter "$NAME")
        [[ "$L" ]] || continue
        A=$(get_opt_arg "$NAME")
        [[ "$A" ]] && continue
        FLAGS="$FLAGS$L"
    done
    printf %s "$FLAGS"
}

# prints only long options that take no parameter
print_long_flags() {
    local NAME DESC L A
    local FLAGS=""
    local SPACE=""

    for NAME in ${ARGP_OPTION_LIST:-}; do
        DESC=$(get_opt_desc "$NAME")
        [[ "$DESC" ]] || continue
        L=$(get_opt_string "$NAME")
        [[ "$L" == "$GARGP_ENDOPTS_loption" ]] && continue
        [[ "$L" ]] || continue
        A=$(get_opt_arg "$NAME")
        [[ "$A" ]] && continue
        printf -- "$SPACE--%s" "$L"
        SPACE=" "
    done
    printf %s "$FLAGS"
}

# prints short and long options that take a parameter
print_man_all_args() {
    local NAME FMT
    for NAME in ${ARGP_OPTION_LIST:-}; do
        local DESC
        DESC=$(get_opt_desc "$NAME")
        [[ "$DESC" ]] || continue
        local ARG
        ARG=$(get_opt_arg "$NAME")
        [[ "$ARG" ]] || continue

        local SOPT
        SOPT=$(get_opt_letter "$NAME")
        local LOPT
        LOPT=$(get_opt_string "$NAME")

        echo -n " ["
        [[ "$SOPT" ]] && printf -- '\\fB\-%s\\fP' "$SOPT"
        [[ "$SOPT" && "$LOPT" ]] && echo -n ","
        printf -- '\\fB\-\-%s\\fP=\\fI%s\\fP' "$LOPT" "$ARG"
        echo -n "]"
    done
}

# prints short and long options that take a parameter - only used for
# default usage message
print_all_args() {
    local NAME
    for NAME in ${ARGP_OPTION_LIST:-}; do
        local DESC
        DESC=$(get_opt_desc "$NAME")
        [[ "$DESC" ]] || continue
        local ARG
        ARG=$(get_opt_arg "$NAME")
        [[ "$ARG" ]] || continue

        local SOPT
        SOPT=$(get_opt_letter "$NAME")
        local LOPT
        LOPT=$(get_opt_string "$NAME")

        echo -n " ["
        [[ "$SOPT" ]] && echo -n "-$SOPT"
        [[ "$SOPT" && "$LOPT" ]] && {
            echo -n ","
        }
        [[ "$LOPT" ]] && echo -n "--$LOPT="
        [[ "$ARG" ]] && echo -n "$ARG"
        echo -n "]"
    done
}

# print the help line for all options
print_all_opts() {
    local NAME
    for NAME in ${ARGP_OPTION_LIST:-}; do
        print_opt "$NAME"
    done
}

# print the option line for a man page
print_man_opt() {
    local NAME="$1"
    local L DESC SOPT LOPT ARG

    DESC=$(get_opt_desc "$NAME")
    [[ "$DESC" ]] || return 0
    SOPT=$(get_opt_letter "$NAME")
    LOPT=$(get_opt_string "$NAME")
    ARG=$(get_opt_arg "$NAME")

    echo ".TP"
    echo -n ".B "
    # NB 'echo -n "-E"' swallows the -E!! and it has no --
    [[ "$SOPT" ]] && printf -- '\\fB\-%s\\fP' "$SOPT"
    if [[ "$GARGP_LONG_GETOPT" ]]; then
        if [[ "$LOPT" != "$GARGP_ENDOPTS_loption" ]]; then
            [[ "$SOPT" && "$LOPT" ]] && echo -n ", "
            [[ "$LOPT" ]] && printf -- '\\fB\-\-%s\\fP' "$LOPT"
        fi
    fi
    [[ "$ARG" ]] && echo -n " \\fI$ARG\\fR"
    echo
    echo "$DESC"
}

# Create a skeleton man page - uses these parameters if defined:
# USAGE
# ARGP_ARGS
# SHORT_DESC
print_man_page() {
    local FLAGS
    FLAGS=$(print_short_flags)
    [[ "$FLAGS" ]] && FLAGS='
.RB "[" \-'$FLAGS' "]"'
    local LFLAGS
    LFLAGS=$(print_long_flags | sed 's/-/\\-/g')
    [[ "$LFLAGS" ]] && LFLAGS='
[
.B '$LFLAGS'
]'
    local ARGS
    ARGS=$( print_man_all_args )

    # shellcheck disable=SC2018
    # shellcheck disable=SC2019
    cat <<EOF
.TH $( echo "$ARGP_PROG"|tr 'a-z' 'A-Z' ) 1 \" -*- nroff -*-
.SH NAME
$ARGP_PROG \- $SHORT_DESC
.SH SYNOPSIS
.hy 0
.na
.B $ARGP_PROG$FLAGS$LFLAGS
$ARGS
${ARGP_ARGS:-}
.ad b
.hy 0
.SH DESCRIPTION
.nf
${USAGE:-}
.P
.SH OPTIONS
EOF

    local NAME
    for NAME in ${ARGP_OPTION_LIST:-}; do
        print_man_opt "$NAME"
    done

    cat <<EOF
.SH "EXIT STATUS"
.SH "ENVIRONMENT"
.SH "FILES"
.SH "EXAMPLES"
.SH "NOTES"
.SH "BUGS"
.SH "SEE ALSO"
.SH "AUTHOR"
Written by Foo Bar <foobar@foobar.org>
.P
.RB http://foobar.foobar.org/foobar
.SH "COPYRIGHT"
Copyright (c) 2012 Foo Bar
.br
This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.
.P
This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.
.P
You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301, USA
EOF
}

escape() {
    sed 's/</\&lt;/g; s/>/\&gt;/g'
}

print_xml() {
    local NAME

    echo '<?xml version="1.0" encoding="UTF-8"?>'
    echo "<argp><prog>$ARGP_PROG</prog>"
    echo "<args>$ARGP_ARGS</args>"
    echo "<short>$SHORT_DESC</short>"
    echo "<version>$GARGP_VERSION</version>"
    echo "<usage>$( echo "$USAGE" | escape )</usage>"
    for NAME in ${ARGP_OPTION_LIST:-}; do
        case $(convert_to_option_name "$NAME") in
            $GARGP_PRINTXML_loption| \
            $GARGP_PRINTMAN_loption| \
            $GARGP_HELP_loption| \
            $GARGP_ENDOPTS_loption)
                continue;
        esac
        echo "<option name='$NAME' sname='$(get_opt_letter "$NAME")' type='$(get_opt_type "$NAME")' arg='$(get_opt_arg "$NAME")' default='$(get_opt_default "$NAME")' range='$(get_opt_range "$NAME")'>$(get_opt_raw_desc "$NAME")</option>"
    done
    echo "</argp>"
}

add_std_opts() {
    get_opt_name -"$GARGP_HELP_option" >/dev/null && GARGP_HELP_option=
    get_opt_name --"$GARGP_HELP_loption"  >/dev/null && GARGP_HELP_loption=
    if [[ "$GARGP_HELP_option$GARGP_HELP_loption" ]]; then
        add_opt "$GARGP_HELP_loption" "" "$GARGP_HELP_option" "" b "" "print this help and exit"
    fi

    get_opt_name -"$GARGP_VERSION_option"  >/dev/null && GARGP_VERSION_option=
    get_opt_name --"$GARGP_VERSION_loption"  >/dev/null && GARGP_VERSION_loption=
    if [[ "$GARGP_VERSION_option$GARGP_VERSION_loption" ]]; then
        add_opt "$GARGP_VERSION_loption" "" "$GARGP_VERSION_option" "" b "" "print version and exit"
    fi

    get_opt_name -"$GARGP_VERBOSE_option" >/dev/null  && GARGP_VERBOSE_option=
    get_opt_name --"$GARGP_VERBOSE_loption" >/dev/null  && GARGP_VERBOSE_loption=
    if [[ "$GARGP_VERBOSE_option$GARGP_VERBOSE_loption" ]]; then
        add_opt "$GARGP_VERBOSE_loption" "${VERBOSE:-}" "$GARGP_VERBOSE_option" "" b "" "do it verbosely"
    fi

    get_opt_name -"$GARGP_QUIET_option" >/dev/null  && GARGP_QUIET_option=
    get_opt_name --"$GARGP_QUIET_loption"  >/dev/null && GARGP_QUIET_loption=
    if [[ "$GARGP_QUIET_option$GARGP_QUIET_loption" ]]; then
        add_opt "$GARGP_QUIET_loption" "${QUIET:-}" "$GARGP_QUIET_option" "" b "" "do it quietly"
    fi

    add_opt "$GARGP_PRINTMAN_loption"
    add_opt "$GARGP_PRINTXML_loption"
    add_opt "$GARGP_ENDOPTS_loption" "" "-" "" b "" "explicitly ends the options"
}

print_opt() {
    local NAME="$1"
    local L N DESC SOPT LOPT ARG

    DESC=$(get_opt_desc "$NAME")
    [[ "$DESC" ]] || return 0
    SOPT=$(get_opt_letter "$NAME")
    LOPT=$(get_opt_string "$NAME")
    ARG=$(get_opt_arg "$NAME")

    LINE=""
    for (( N=0 ; ${#LINE} < GARGP_SHORT_OPT_COL ; N++ )) ; do
        LINE="$LINE "
    done
    [[ "$SOPT" ]] && LINE="${LINE}-$SOPT"
    if [[ "$GARGP_LONG_GETOPT" && "$LOPT" != "$GARGP_ENDOPTS_loption" ]]; then
        [[ "$SOPT" && "$LOPT" ]] && LINE="$LINE, "
        if [[ "$LOPT" ]]; then
            for (( N=0 ; ${#LINE} < GARGP_LONG_OPT_COL ; N++ )); do
                LINE="$LINE "
            done
            [[ "$LOPT" ]] && LINE="${LINE}--$LOPT"
            [[ "$LOPT" && "$ARG" ]] && LINE="$LINE="
        fi
    fi
    [[ "$ARG" ]] && LINE="${LINE}$ARG"

    LINE="$LINE "
    while (( ${#LINE} < GARGP_OPT_DOC_COL - 1)) ; do LINE="$LINE " ; done
    # NB 'echo "-E"' swallows the -E!! and it has no -- so use printf
    printf -- "%s" "$LINE"

    FIRST="FIRST_yes"
    if (( ${#LINE} >= GARGP_OPT_DOC_COL )); then
        echo
        FIRST=""
    fi
    local WIDTH=$(( GARGP_RMARGIN - GARGP_OPT_DOC_COL ))
    if ! type fmt &> /dev/null || [[ "$WIDTH" -lt 10 ]]; then
        printf -- "%s\n" "$DESC"
        return 0
    fi

    export ARGP_INDENT=""
    while (( ${#ARGP_INDENT} < GARGP_OPT_DOC_COL - 1)); do
        ARGP_INDENT="$ARGP_INDENT "
    done
    echo "$DESC" | fmt -w "$WIDTH" -s |
    while read -r L; do
        [[ "$FIRST" ]] || printf %s "$ARGP_INDENT"
        FIRST=""
        printf -- "%s\n" "$L"
    done
    unset ARGP_INDENT
}

# honour GNU ARGP_HELP_FMT parameter
load_help_fmt() {
    [[ "${ARGP_HELP_FMT:-}" ]] || return 0
    OFS="$IFS"
    IFS=','
    # shellcheck disable=SC2086
    set -- $ARGP_HELP_FMT
    IFS="$OFS"
    while [[ "$1" ]]; do
        case "$1" in
            short-opt-col*)
                GARGP_SHORT_OPT_COL=$(echo "$1"|cut -d'=' -f 2)
                shift
                ;;
            long-opt-col*)
                GARGP_LONG_OPT_COL=$(echo "$1"|cut -d'=' -f 2)
                shift
                ;;
            opt-doc-col*)
                GARGP_OPT_DOC_COL=$(echo "$1"|cut -d'=' -f 2)
                shift
                ;;
            rmargin*)
                GARGP_RMARGIN=$(echo "$1"|cut -d'=' -f 2)
                shift
                ;;
            *)
                shift
                ;;
        esac
    done
}

default_usage() {
    local FLAGS
    FLAGS=$( print_short_flags )
    [[ "$FLAGS" ]] && FLAGS="[-$FLAGS]"
    local LFLAGS
    LFLAGS=$( print_long_flags )
    [[ "$LFLAGS" ]] && LFLAGS=" [$LFLAGS]"
    local ARGS
    ARGS=$( print_all_args )
    local FMT
    FMT="fmt -w $GARGP_RMARGIN -s"
    type fmt &> /dev/null || FMT=cat
    echo -e "Usage: $ARGP_PROG $FLAGS$LFLAGS $ARGS ${ARGP_ARGS:-}" |$FMT
    echo
    echo "${USAGE:-}"
    echo
    echo "Options:"
    echo
    print_all_opts
}

range_check_entry() {
    local NAME="$1"
    local VALUE="$2"
    local TYPE="$3"
    local RANGE="$4"

    [[ "$ARGP_DEBUG" ]] && echo "${FUNCNAME[0]}: VALUE='$VALUE' TYPE='$TYPE' RANGE='$RANGE'" >&2
    # just using 'while' for the sake of the 'break':
    while [[ "$TYPE" ]]; do
        case "$TYPE" in
            i)
                [[ "$VALUE" =~ $GARGP_INT_REGEX ]] || break
                [[ "$RANGE" ]] && {
                    [[ "$RANGE" =~ $GARGP_INT_RANGE_REGEX ]] || break
                    LOWER=${BASH_REMATCH[1]}
                    UPPER=${BASH_REMATCH[2]}
                    [[ "$LOWER" && "$VALUE" -lt "$LOWER" ]] && break
                    [[ "$UPPER" && "$VALUE" -gt "$UPPER" ]] && break
                }
                return 0
                ;;
            r|f|d)
                [[ "$VALUE" =~ $GARGP_FLOAT_REGEX ]] || break
                [[ "$RANGE" ]] && {
                    [[ "$RANGE" =~ $GARGP_FLOAT_RANGE_REGEX ]] || break
                    LOWER=${BASH_REMATCH[1]}
                    UPPER=${BASH_REMATCH[3]}
                    [[ "$LOWER" ]] && {
                        awk "BEGIN {if ($VALUE < $LOWER) {exit 1} else {exit 0}}" || break
                    }
                    [[ "$UPPER" ]] && {
                        awk "BEGIN {if ($VALUE > $UPPER) {exit 1} else {exit 0}}" || break
                    }
                }
                return 0
                ;;
            s*)
                [[ "$RANGE" ]] && {
                    [[ "$VALUE" =~ $RANGE ]] || break
                }
                return 0
                ;;
            a*)
                local VAL
                SEP=${TYPE:1:1}
                [[ "$SEP" ]] && IFS=$SEP
                for VAL in $RANGE; do
                    [[ "$VAL" == "$VALUE" ]] && IFS="$GARGP_DEFAULT_IFS" && return 0
                done
                IFS="$GARGP_DEFAULT_IFS"
                break
                ;;
        esac
    done

    MSG="$ARGP_PROG: value '$VALUE' given for option '$NAME'"
    if [[ "$TYPE" != s* ]]; then
        MSG+=" must be of type '$TYPE'"
        [[ "$RANGE" ]] && MSG+=" and"
    fi
    [[ "$RANGE" ]] && {
        case "$TYPE" in
            s*)
                MSG+=" must fit the regex '$RANGE'"
                ;;
            a*)
                MSG+=" must be one of these values: '$RANGE'"
                ;;
            r|f|d|i)
                MSG+=" must be in the range '$RANGE'"
                ;;
        esac
    }
    abend 1 "$MSG"
}

get_opt_name() {
    # returns the name for an option letter or word
    local OPT="$1" # an option eg -c or --foobar
    local NAME
    for NAME in ${ARGP_OPTION_LIST:-}; do
        local ARGP_L=ARGP_LOPT_$NAME
        local ARGP_S=ARGP_SOPT_$NAME
        if  [[ "--${!ARGP_L:-}" = "$OPT" ]] || \
            [[ "-${!ARGP_S:-}" = "$OPT" ]]; then
            echo "${NAME}"
            return 0
        fi
    done
    return 1
}

process_opts() {
    [[ "$ARGP_DEBUG" ]] && debug_args "$@"
    local SHIFT_NUM=0 OPTION NAME TYPE RANGE WAS_SET

    while true; do
        OPTION="${1:-}"
        if  [[ "$GARGP_HELP_option"  &&  "-$GARGP_HELP_option"  == "$OPTION" ]] ||
            [[ "$GARGP_HELP_loption" && "--$GARGP_HELP_loption" == "$OPTION" ]]; then
            usage 2>/dev/null || default_usage
            echo "exit 0;" >&3
            exit 0
        fi

        if  [[ "$GARGP_VERSION_option"  &&  "-$GARGP_VERSION_option"  == "$OPTION" ]] ||
            [[ "$GARGP_VERSION_loption" && "--$GARGP_VERSION_loption" == "$OPTION" ]]; then
            echo "echo $ARGP_PROG: version: '$ARGP_VERSION'; exit 0;" >&3
            exit 0
        fi

        if [[ "$GARGP_PRINTMAN_loption" && --$GARGP_PRINTMAN_loption == "$OPTION" ]]; then
            print_man_page
            echo "exit 0;" >&3
            exit 0
        fi

        if [[ "$GARGP_PRINTXML_loption" && --$GARGP_PRINTXML_loption == "$OPTION" ]]; then
            print_xml
            echo "exit 0;" >&3
            exit 0
        fi

        ((SHIFT_NUM++))
        shift

        [[ "$OPTION" == "--" ]] && break

        # here is where all the user options get done:
        NAME=$(get_opt_name "$OPTION")
        [[ "$NAME" ]] || {
            abend 1 "$ARGP_PROG: argp.sh: no name for option \"$OPTION\""
        }

        TYPE=$(get_opt_type "$NAME")
        [[ "$TYPE" ]] || {
            abend 1 "$ARGP_PROG: argp.sh: no type for option \"$OPTION\""
        }
        RANGE=$(get_opt_range "$NAME")

        WAS_SET=$(get_opt_was_set "$NAME")

        [[ "$ARGP_DEBUG" ]] &&
            echo "process_opts: option='$OPTION' name='$NAME' type='$TYPE' range='$RANGE' was_set='$WAS_SET' value='${!NAME}'"
        case $TYPE in
            b)
                if [[ "$RANGE" ]]; then
                    if [[ -z "${!NAME}" ]]; then
                        export "$NAME"="$RANGE"
                    else
                        export "$NAME="
                    fi
                else
                    if [[ "${!NAME}" =~ ^[0-9]+$ ]]; then
                        export "$NAME"=$(( NAME + 1 ))
                    else
                        if [[ -z "${!NAME}" ]]; then
                            export "$NAME=set"
                        else
                            export "$NAME="
                        fi
                    fi
                fi
                ;;
            *)
                local VALUE="$1"
                [[ "$RANGE" ]] &&
                range_check_entry "$NAME" "$VALUE" "$TYPE" "$RANGE"
                case $TYPE in
                    a*|s*)
                        local SEP=${TYPE:1:1}
                        [[ "$SEP" ]] || SEP="$ARGP_OPTION_SEP"
                        if [[ "$WAS_SET" && "$SEP" && "${!NAME}" ]]; then
                            export "$NAME=${!NAME}${SEP}$VALUE"
                        else
                            export "$NAME=$VALUE"
                        fi
                        ;;
                    *)
                        export "$NAME"="$VALUE"
                        ;;
                esac
                ((SHIFT_NUM++))
                shift
                set +x
                ;;
        esac
        export ARGP_WAS_SET_$NAME="yes"

    done
    return $SHIFT_NUM
}


output_values() {
    [[ "$ARGP_DEBUG" ]] && debug_args "$@"
    local NAME VALUE WAS_SET
    for NAME in $ARGP_OPTION_LIST; do
        VALUE="${!NAME}"
        WAS_SET=$(get_opt_was_set "$NAME")
        [[ "$WAS_SET" ]] || {
            VALUE=$(get_opt_default "$NAME")
        }
        VALUE="${VALUE//\\/\\\\}"
        VALUE="${VALUE//\'/\\\'}"
        echo -n "export $ARGP_PREFIX$NAME=\$'$VALUE'; "
    done

    echo -n "set -- "
    for VALUE in "$@"; do
        VALUE="${VALUE//\\/\\\\}"
        VALUE="${VALUE//\'/\\\'}"
        echo -n " \$'$VALUE'"
    done
    echo
}

call_getopt() {
    [[ "$ARGP_DEBUG" ]] && debug_args "$@"

    local SHORT_OPTIONS=""
    local SHORT_OPTIONS_ARG=""
    local LONG_OPTIONS=""
    local LONG_OPTIONS_ARG=""
    local STOP_EARLY=""
    local OPT TEMP NAME LONG ARG

    for NAME in $ARGP_OPTION_LIST; do
        OPT=$(get_opt_letter "$NAME")
        ARG=$(get_opt_arg "$NAME")
        LONG=$(get_opt_string "$NAME")
        [[ "$OPT" == '-' ]] && continue
        if [[ "$OPT" ]]; then
            if [[ "$ARG" ]]; then
                SHORT_OPTIONS_ARG+="$OPT:"
            else
                SHORT_OPTIONS+="$OPT"
            fi
        fi

        if [[ "$ARG" ]]; then
            [[ "$LONG_OPTIONS_ARG" ]] && LONG_OPTIONS_ARG+=","
            LONG_OPTIONS_ARG+="$LONG:"
        else
            [[ "$LONG_OPTIONS" ]] && LONG_OPTIONS+=","
            LONG_OPTIONS+="$LONG"
        fi
    done

    [[ "$STOP_ON_FIRST_NON_OPT" ]] && STOP_EARLY="+"
    if [[ "${GARGP_LONG_GETOPT:-''}" ]]; then
        local SHORT_ARGS=""
        local LONG_ARGS=""
        [[ "$SHORT_OPTIONS$SHORT_OPTIONS_ARG" ]] && SHORT_ARGS="-o $STOP_EARLY$SHORT_OPTIONS$SHORT_OPTIONS_ARG"
        [[ "$LONG_OPTIONS" ]] && LONG_ARGS="--long $LONG_OPTIONS"
        [[ "$LONG_OPTIONS_ARG" ]] && LONG_ARGS="$LONG_ARGS --long $LONG_OPTIONS_ARG"
        [[ "$ARGP_DEBUG" ]] && echo "call_getopt: set -- \$(getopt $SHORT_ARGS $LONG_ARGS -n $ARGP_PROG -- $*)" >&2
        # shellcheck disable=SC2086
        TEMP=$(getopt $SHORT_ARGS $LONG_ARGS -n "$ARGP_PROG" -- "$@") || abend $? "getopt failure"
    else
        [[ "$ARGP_DEBUG" ]] && echo "call_getopt: set -- \$(getopt $SHORT_OPTIONS$SHORT_OPTIONS_ARG $*)" >&2
        # shellcheck disable=SC2086
        TEMP=$(getopt $SHORT_OPTIONS$SHORT_OPTIONS_ARG "$@") || abend $? "getopt failure"
    fi

    eval set -- "$TEMP"

    [[ "$ARGP_DEBUG" ]] && debug_args "$@"

    ARGS=( "$@" )
}

sort_option_names_by_key()
{
    [[ "$ARGP_DEBUG" ]] && echo "sort_option_names_by_key: before: $ARGP_OPTION_LIST" >&2
    local NEW_OPTION_LIST NAME KEY
    local TMP
    TMP=$( mktemp )
    # shellcheck disable=SC2064
    trap "rm -f $TMP" RETURN

    for NAME in ${ARGP_OPTION_LIST:-}; do
        KEY=$(get_opt_letter "$NAME")
        [[ "$KEY" ]] || KEY="~" # ie collate last
        [[ "$KEY" == - ]] && KEY="~" # ie collate last
        echo "$KEY $NAME"
    done | sort --ignore-case > "$TMP"

    while read -r KEY NAME; do
        NEW_OPTION_LIST+="$NAME "
    done < "$TMP"

    ARGP_OPTION_LIST="$NEW_OPTION_LIST"
    [[ "$ARGP_DEBUG" ]] && echo "sort_option_names_by_key: after: $NEW_OPTION_LIST" >&2
}

initialise() {
    GARGP_VERSION="2.6"
    GARGP_DEFAULT_IFS=$' \t\n'
    IFS="$GARGP_DEFAULT_IFS"
    STOP_ON_FIRST_NON_OPT=${POSIXLY_CORRECT:-}
    unset POSIXLY_CORRECT

    ARGP_OPTION_LIST=""
    ARGP_OPTION_SEP=
    GARGP_LONG_GETOPT=""
    # decide if this getopt supports long options:
    {
        getopt --test &>/dev/null; ARGP_STAT=$?
    } || :
    [[ $ARGP_STAT -eq 4 ]] && GARGP_LONG_GETOPT="GARGP_LONG_GETOPT_yes"

    GARGP_HELP_loption="help"
    GARGP_HELP_option="h"
    GARGP_VERBOSE_loption="verbose"
    GARGP_VERBOSE_option="v"
    VERBOSE=${VERBOSE:-}
    GARGP_QUIET_option="q"
    GARGP_QUIET_loption="quiet"
    QUIET=${QUIET:-}
    GARGP_VERSION_loption="version"
    GARGP_VERSION_option="V"
    GARGP_PRINTMAN_loption="print-man-page"
    GARGP_PRINTXML_loption="print-xml"
    GARGP_ENDOPTS_loption="end-all-options"

    GARGP_SHORT_OPT_COL=2
    GARGP_LONG_OPT_COL=6
    GARGP_OPT_DOC_COL=29

    GARGP_INT_REGEX="[+-]*[[:digit:]]+"
    GARGP_INT_RANGE_REGEX="($GARGP_INT_REGEX)*[-:]($GARGP_INT_REGEX)*"
    GARGP_FLOAT_REGEX="[+-]*[[:digit:]]+(\\.[[:digit:]]+)*"
    GARGP_FLOAT_RANGE_REGEX="($GARGP_FLOAT_REGEX)[-:]($GARGP_FLOAT_REGEX)"
    # FIXME: this needs a few tweaks:
    GARGP_URL_REGEX="(nfs|http|https|ftp|file)://[[:alnum:]_.-]*[^[:space:]]*"

    # cron jobs have TERM=dumb and tput throws errors:
    _GPG_COLUMNS=$( [[ "$TERM" && "$TERM" != "dumb" ]] && tput cols || echo 80)
    GARGP_RMARGIN=$_GPG_COLUMNS

    load_help_fmt
    (( GARGP_RMARGIN > _GPG_COLUMNS )) && GARGP_RMARGIN=$_GPG_COLUMNS

    # we're being called directly from the commandline (possibly in error
    # but maybe the guy is just curious):
    tty -s && {
        ARGP_VERSION=$GARGP_VERSION
        add_std_opts
        ARGP_PROG=${0##*/} # == basename
        ARGP_argp_sh_usage
        exit 0
    }
}

read_xml_format() {
    type xmlstarlet &>/dev/null || abend 1 "Please install xmlstarlet"
    echo "ARGP_DELETE=verbose quiet version"
    xmlstarlet sel --text -t -m '/argp' \
        -v "concat('ARGP_DELETE=', delete)" -n \
        -v "concat('ARGP_VERSION=',version)" -n \
        -v "concat('ARGP_PROG=', prog)" -n \
        -v "concat('ARGP_PREFIX=', prefix)" -n \
        -t -m '/argp/option' \
        -v "concat(@name, \"='\", @default, \"' '\", @sname, \"' '\", @arg, \"' '\", @type, \"' '\", @range, \"' \", self::option)" -n \
        -t -m '/argp' \
        -v "concat('ARGP_ARGS=', args)" -n \
        -v "concat('ARGP_SHORT=', short)" -n \
        -v "concat('ARGP_USAGE=', usage)" -n
}

read_config() {
    # note that we can't use process substitution:
    # foobar < <( barfoo )
    # as POSIXLY_CORRECT disables it! So we'll use a temp file for the xml.
    local TMP
    TMP=$( mktemp )
    # shellcheck disable=SC2064
    trap "rm -f $TMP" EXIT

    local FILE_TYPE LINE
    # shellcheck disable=SC2162
    while read LINE; do
        [[ "$FILE_TYPE" ]] || {
            FILE_TYPE="flat"
            [[ "$LINE" == "<?xml"* ]] && {
                FILE_TYPE="xml"
                {
                    echo "$LINE"
                    cat
                } | read_xml_format > "$TMP"
                exec < "$TMP"
                continue
            }
        }

        [[ "$ARGP_DEBUG" ]] && echo "read: $LINE" >&2
        case "$LINE" in
            "ARGP_PROG="*)
                ARGP_PROG="${LINE#ARGP_PROG=}"
                ;;
            "ARGP_DELETE="*)
                del_opt "${LINE#ARGP_DELETE=}"
                ;;
            "ARGP_ARGS="*)
                ARGP_ARGS="${LINE#ARGP_ARGS=}"
                ;;
            "ARGP_SHORT="*)
                SHORT_DESC="${LINE#ARGP_SHORT=}"
                ;;
            "ARGP_VERSION="*)
                ARGP_VERSION="${LINE#ARGP_VERSION=}"
                ;;
            "ARGP_OPTION_SEP="*)
                ARGP_OPTION_SEP="${LINE#ARGP_OPTION_SEP=}"
                ;;
            "ARGP_PREFIX="*)
                ARGP_PREFIX="${LINE#ARGP_PREFIX=}"
                ;;
            "ARGP_USAGE="*)
                if [[ "${LINE#ARGP_USAGE=}" ]]; then
                    USAGE="${LINE#ARGP_USAGE=} "$'\n'
                else
                    USAGE=
                fi
                USAGE+=$(cat)
                break
                ;;
            [A-Za-z]*=*)
                local NAME REGEX DEFAULT SNAME ARG TYPE RANGE DESC VAR
                NAME="${LINE%%=*}"
                LINE="${LINE#$NAME=}"
                NAME=$(convert_to_env_name "$NAME")
                # initial value could contain spaces, quotes, anything -
                # but I don't think we need to support escaped quotes:
                REGEX="^[[:space:]]*('[^']*'|[^[:space:]]+)[[:space:]]*(.*)"
                for VAR in DEFAULT SNAME ARG TYPE RANGE; do
                    [[ "$LINE" =~ $REGEX ]] || break
                    V="${BASH_REMATCH[1]}"
                    V="${V%\'}"
                    V="${V#\'}"
                    local "$VAR"="$V"
                    LINE="${BASH_REMATCH[2]}"
                done
                DESC="$LINE"
                while [[ "$DESC" == *\\ ]]; do
                    DESC="${DESC%\\}"
                    read LINE
                    DESC+="$LINE"
                    [[ "$ARGP_DEBUG" ]] && echo "read for DESC: $LINE" >&2
                done
                add_opt "$NAME" "$DEFAULT" "$SNAME" "$ARG" "$TYPE" "$RANGE" "$DESC"
                ;;
            *) # includes comments
                ;;
        esac
    done
}

main() {
    add_std_opts

    read_config

    sort_option_names_by_key

    call_getopt "$@"

    [[ "$ARGP_DEBUG" ]] && debug_args "$@"

    process_opts "${ARGS[@]}"
    ARGP_NUM_SHIFT=$?
    set -- "${ARGS[@]}"
    shift $ARGP_NUM_SHIFT

    [[ "$ARGP_DEBUG" ]] && debug_args "$@"

    output_values "$@" >&3
}

check_bash() {
    # don't assume bash-3+ yet
    [ -n "$ARGP_DEBUG" ] && echo "$ARGP_PROG: arpg.sh: debug is on" >&2

    ARGP_GOOD_ENOUGH=""
    if [ "x$BASH_VERSION" = "x" ] || [ "x${BASH_VERSINFO[*]}" = "x" ]; then
        :
    elif [ "${BASH_VERSINFO[0]}" -gt 2 ]; then
        ARGP_GOOD_ENOUGH="1"
    fi
    if [ "x$ARGP_GOOD_ENOUGH" = "x" ]; then
        echo "$0: This version of the shell does not support this program." >&2
        echo "bash-3 or later is required" >&2
        echo "exit 1;" >&3
        exit 1
    fi
}

try_c_version() {
    # don't assume bash-3+ yet
    # shellcheck disable=SC2006
    OVERRIDE_PROCESSOR=""
    [ -z "$ARGP_PROCESSOR" ] && ARGP_PROCESSOR=`which argp 2>/dev/null`
    [ -n "$ARGP_PROCESSOR" ] && OVERRIDE_PROCESSOR=`basename "$ARGP_PROCESSOR"`
    # shellcheck disable=SC2006
    THIS=`basename "$0"`

    [ "$OVERRIDE_PROCESSOR" != "$THIS" ] && type "$ARGP_PROCESSOR" &>/dev/null && {
        [ -n "$ARGP_DEBUG" ] &&
        echo "$ARGP_PROG: $THIS exec'ing argp: you can use ARGP_PROCESSOR to override this" >&2
        exec "$ARGP_PROCESSOR" "$@"
    }
}

try_c_version "$@"
check_bash
initialise
main "$@"

# just to make sure we don't return with non-zero $?:
:
