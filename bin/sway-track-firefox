#!/bin/bash

# Copyright (C) 2020-2021 Bob Hepple <bob.hepple@gmail.com>

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or (at
# your option) any later version.
# 
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
# General Public License for more details.
# 
# You should have received a copy of the GNU General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

# http://bhepple.freeshell.org

case $1 in
    -h|--help)
        echo "$( basename $0 ): when firefox gets focus, bind Shift+Insert to paste PRIMARY
when firefox loses focus, unbind Shift+Insert"
        exit 0
        ;;
esac

PROG=$( basename $0 )
VERBOSE="" # set to 'non-null' to debug

[[ "$1" == "doit" ]] && {
    [[ "$VERBOSE" ]] && echo "$PROG: sway-track-firefox doit"
    #sudo ydotool type --delay 700 -- "$( wl-paste --no-newline --primary --type text/plain )"
    sudo ydotool click middle
    [[ "$VERBOSE" ]] && echo "$PROG: sway-track-firefox: ydotool complete"
    exit 0
}

swaymsg -m -t subscribe '["window"]'  |
jq --unbuffered -r 'select(.change == "focus") | (.container.app_id, .container.window_properties.class)' |
while read new_app; do
    read class
    [[ "$VERBOSE" ]] && echo "$( date ): $PROG: app_id=$new_app / x11_class=$class focused"
    bindit=no
    case "$new_app" in
        "firefox"|"libreoffice"*|"gjots2"|"org.gnome."*) # add in any other misbehaving apps
            bindit=yes
            ;;
        "null") # X11 applications:
            case $class in
                "Diffuse")
                    bindit=yes
                    ;;
            esac
            ;;
        *)
            bindit=no
            ;;
    esac
    if [[ $bindit == "yes" ]]; then
        [[ "$VERBOSE" ]] && echo "$PROG: binding Shift Insert"
        swaymsg -- 'bindsym Shift+Insert exec sway-track-firefox doit' &> /dev/null
    else
        [[ "$VERBOSE" ]] && echo "$PROG: unbinding Shift Insert"
        swaymsg -- "unbindsym Shift+Insert" &> /dev/null
    fi
done
